# do vault variables get masked in environment:url: ?

## vault setup

- setup vault in docker on a runner host, no tls for ease
- prod mode 

```
# vault secrets enable --path=secrets kv
```

```
# vault kv put -mount=secrets production base_url=example.com
Success! Data written to: secrets/production
```

```
# vault kv get -mount=secrets production
====== Data ======
Key         Value
---         -----
base_url    example.com
```

```
# vault policy write masked-test - <<EOF
path "secrets/production/*" {
  capabilities = [ "read" ]
}
EOF
```

```
# vault auth enable jwt
```

```
# vault write auth/jwt/config \
    oidc_discovery_url="https://gitlab.com" \
    bound_issuer="https://gitlab.com"
```

```
# vault write auth/jwt/role/masked-test - <<EOF
{
  "role_type": "jwt",
  "policies": ["masked-test"],
  "token_explicit_max_ttl": 60,
  "user_claim": "user_email",
  "bound_claims": {
    "project_id": "22",
    "ref": "main",
    "ref_type": "branch"
  }
}
EOF
```